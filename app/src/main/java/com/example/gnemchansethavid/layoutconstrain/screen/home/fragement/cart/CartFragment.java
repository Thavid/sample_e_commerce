package com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.cart;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.gnemchansethavid.layoutconstrain.R;
import com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.cart.adapter.CartAdapter;
import com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.cart.entity.Cart;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class CartFragment extends Fragment {
    private List<Cart> listData = new ArrayList();
    private CartAdapter adapter;
    private RecyclerView cartRecyclerView;
    private ImageView btnDelete;
    private CheckBox checkAll;
    public CartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        cartRecyclerView = view.findViewById(R.id.cartRecyclerview);
        checkAll = view.findViewById(R.id.selectAll);
        btnDelete = view.findViewById(R.id.btnDelete);
        adapter = new CartAdapter(listData,getContext(),checkAll);

        for(int i=0;i<10;i++){
            listData.add(new Cart(R.drawable.man_clothes,"Man Jacket"+i,"$ 16.99", "25-12-2018",false));
        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(),LinearLayout.VERTICAL,false);
        cartRecyclerView.setLayoutManager(layoutManager);
        cartRecyclerView.setAdapter(adapter);

        checkAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listData.size()!=0){
                    if(checkAll.isChecked()){
                        for(int i=0;i<listData.size();i++){
                            listData.get(i).setCheck(true);
                            adapter.addAllCheck(true);
                        }
                    }else {
                        for(int i=0;i<listData.size();i++){
                            listData.get(i).setCheck(false);
                            adapter.addAllCheck(false);
                        }
                    }

                }else {
                    checkAll.setChecked(false);
                }
                adapter.notifyDataSetChanged();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure?")
                        .setContentText("Won't be able to recover this file!")
                        .setCancelText("Cancel")
                        .setConfirmText("Delete!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                adapter.deleteCart(listData);
                                checkAll.setChecked(false);
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        for(int i=0;i<listData.size();i++){
            listData.get(i).setCheck(false);
            adapter.addAllCheck(false);
            checkAll(false,checkAll);
            adapter.notifyDataSetChanged();
        }
    }

    public void checkAll(boolean b, CheckBox checkAll) {
        checkAll.setChecked(b);
    }
}
