package com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.cart.entity;

public class Cart {
    private int imageProduct;
    private String productName;
    private String productModel;
    private String cartDate;
    private boolean check;

    public Cart(int imageProduct, String productName, String productModel, String cartDate, boolean check) {
        this.imageProduct = imageProduct;
        this.productName = productName;
        this.productModel = productModel;
        this.cartDate = cartDate;
        this.check = check;
    }

    public int getImageProduct() {
        return imageProduct;
    }

    public void setImageProduct(int imageProduct) {
        this.imageProduct = imageProduct;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductModel() {
        return productModel;
    }

    public void setProductModel(String productModel) {
        this.productModel = productModel;
    }

    public String getCartDate() {
        return cartDate;
    }

    public void setCartDate(String cartDate) {
        this.cartDate = cartDate;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    @Override
    public String toString() {
        return "Cart{" +
                "imageProduct=" + imageProduct +
                ", productName='" + productName + '\'' +
                ", productModel='" + productModel + '\'' +
                ", cartDate='" + cartDate + '\'' +
                ", check=" + check +
                '}';
    }
}
