package com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.category.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gnemchansethavid.layoutconstrain.R;
import com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.category.entity.Category;

import java.util.List;

public class CategoryLv2Adapter extends RecyclerView.Adapter<CategoryLv2Adapter.MyViewHolder> {
    private Context context;
    private List<Category> listData;

    public CategoryLv2Adapter(Context context, List<Category> listData) {
        this.context = context;
        this.listData = listData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.category2_layout,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {
        Category category = listData.get(i);
        myViewHolder.txtCate2Name.setText(category.getCateName());
        myViewHolder.imgCate2.setImageResource(category.getImage());
        myViewHolder.txtCate2Name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Clickkkk"+i, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txtCate2Name;
        private ImageView imgCate2;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtCate2Name = itemView.findViewById(R.id.txtCate2Name);
            imgCate2 = itemView.findViewById(R.id.imgLogoCate2);
        }
    }
}
