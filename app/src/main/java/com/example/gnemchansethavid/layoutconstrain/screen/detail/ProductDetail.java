package com.example.gnemchansethavid.layoutconstrain.screen.detail;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.gnemchansethavid.layoutconstrain.R;
import com.example.gnemchansethavid.layoutconstrain.screen.home.MainActivity;
import com.example.gnemchansethavid.layoutconstrain.screen.specification.Specification;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;

public class ProductDetail extends AppCompatActivity {
    SliderLayout sliderLayout;
    ImageView btnBack;
    Button btnAddCart;
    Button btnDetail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        btnBack = findViewById(R.id.btnBack);
        btnAddCart = findViewById(R.id.btnAddCart);
        btnDetail = findViewById(R.id.btnDetail);

        sliderLayout = findViewById(R.id.imageSlider);
        sliderLayout.setIndicatorAnimation(SliderLayout.Animations.FILL); //set indicator animation by using SliderLayout.Animations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderLayout.setScrollTimeInSec(10); //set scroll delay in seconds :
        setSliderViews();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ProductDetail.this, "Add Cart", Toast.LENGTH_SHORT).show();
            }
        });

        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductDetail.this,Specification.class);
                startActivity(intent);
            }
        });
    }

    private void setSliderViews() {
        for (int i = 0; i <= 3; i++) {
            SliderView sliderView = new SliderView(this);
            switch (i) {
                case 0:
                    sliderView.setImageUrl("http://image.balancikacambodia.com/DM/Item/45/thumb/28f9de96-44c6-4f92-8608-a07c84d57728.jpg");
                    break;
                case 1:
                    sliderView.setImageUrl("http://image.balancikacambodia.com/DM/Item/45/thumb/6ca98903-a17e-4e31-8976-2b2b6cc22c55.jpg");
                    break;
                case 2:
                    sliderView.setImageUrl("http://image.balancikacambodia.com/DM/Item/45/thumb/9392b3d2-e815-49e1-8d62-3abe492fd7d0.jpg");
                    break;
                case 3:
                    sliderView.setImageUrl("http://image.balancikacambodia.com/DM/Item/138/thumb/1e802f04-e296-4b33-8430-6ce732184daf.jpg");
                    break;
            }

            sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
            final int finalI = i;
            sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(SliderView sliderView) {
                    //Toast.makeText(ProductDetail.this, "This is slider " + (finalI + 1), Toast.LENGTH_SHORT).show();
                }
            });

            //at last add this view in your layout :
            sliderLayout.addSliderView(sliderView);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
