package com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.cart.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gnemchansethavid.layoutconstrain.R;
import com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.cart.CartFragment;
import com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.cart.entity.Cart;

import java.util.ArrayList;
import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {
    private List<Cart> listData;
    private int checkList = 0;
    private List<Cart> listCheck = new ArrayList<>();
    private Context context;
    private CartFragment cartFragment = new CartFragment();
    private CheckBox checkAll;

    public CartAdapter(List<Cart> listData, Context context,CheckBox checkAll) {
        this.listData = listData;
        this.context = context;
        this.checkAll = checkAll;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cart_layout,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, final int i) {
        Cart cart = listData.get(i);
        myViewHolder.productImg.setImageResource(cart.getImageProduct());
        myViewHolder.txtName.setText(cart.getProductName());
        myViewHolder.txtModel.setText(cart.getProductModel());
        myViewHolder.txtDate.setText(cart.getCartDate());

        checkBox(myViewHolder.checkBox,i);
        if(listData.get(i).isCheck()){
            myViewHolder.checkBox.setChecked(true);
        }else {
            myViewHolder.checkBox.setChecked(false);
        }

        //check main check box if all sub-checkBox are checked
        for(int x=0;x<listData.size();x++){
           if(listData.get(x).isCheck()){
               checkList++;
           }else {
               checkList=0;
           }
        }
        if(checkList <listCheck.size()){
            cartFragment.checkAll(false,checkAll);
        }else {
            if(listCheck.size()>0)
            cartFragment.checkAll(true,checkAll);
        }
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView productImg;
        private TextView txtName,txtModel,txtDate;
        private CheckBox checkBox;
        private ConstraintLayout cart;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            productImg = itemView.findViewById(R.id.productImg);
            txtName = itemView.findViewById(R.id.txtName);
            txtModel = itemView.findViewById(R.id.txtModel);
            txtDate = itemView.findViewById(R.id.txtDate);
            checkBox = itemView.findViewById(R.id.chkItem);
            cart = itemView.findViewById(R.id.cartItem);
        }
    }

    public void checkBox(final CheckBox checkBox, final int i){
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkBox.isChecked()){
                    listData.get(i).setCheck(true);
                    checkBox.setChecked(false);
                    // Check and add to list
                    if(listCheck.size()>0){
                        for(int a=0;a<listCheck.size();a++){
                            if(!listCheck.get(a).equals(listData.get(i).getProductName())){
                                checkList ++;
                            }
                        }
                        if(checkList == listCheck.size()){
                            listCheck.add(listData.get(i));
                            checkList = 0;
                        }
                    }else {
                        listCheck.add(listData.get(i));
                    }
                    // end Check and add to list
                }else {
                    listData.get(i).setCheck(false);
                    checkBox.setChecked(false);
                    //Remove uncheck from list
                    if(listCheck.size()>0){
                        for(int b=0;b<listCheck.size();b++){
                            if(listCheck.get(b).equals(listData.get(i).getProductName())){
                                listCheck.remove(b);
                                checkList = 0;
                            }
                        }
                    }
                    cartFragment.checkAll(false,checkAll);
                }
                notifyDataSetChanged();
            }
        });
    }

    public void addAllCheck(boolean addAll){
        listCheck.clear();
        if(addAll){
            for(Cart cart:listData){
                listCheck.add(cart);
            }
        }else {
            listCheck.clear();
        }
    }

    public void deleteCart(List<Cart> listCart){
        listCart.removeAll(listCheck);
        notifyDataSetChanged();
    }
}
