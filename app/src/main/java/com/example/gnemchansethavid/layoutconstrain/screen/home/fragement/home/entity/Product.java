package com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.home.entity;

public class Product {
   private String name;
   private String code;
   private String price;
   private int image;

    public Product(String name, String code, String price,int image) {
        this.name = name;
        this.code = code;
        this.price = price;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
