package com.example.gnemchansethavid.layoutconstrain.screen.home;

import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.example.gnemchansethavid.layoutconstrain.R;
import com.example.gnemchansethavid.layoutconstrain.screen.home.adapter.ViewPagerAdapter;
import com.example.gnemchansethavid.layoutconstrain.screen.home.entity.NonSwipeViewPager;
import com.example.gnemchansethavid.layoutconstrain.screen.home.entity.TabLayoutData;
import com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.account.AccountFragment;
import com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.cart.CartFragment;
import com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.category.CategoryFragment;
import com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.home.HomeFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    /*SliderLayout sliderLayout;*/
    private TabLayout tabLayout;
    private NonSwipeViewPager viewPager;
    private ArrayList<TabLayoutData> tabData;
    private ViewPagerAdapter pagerAdater;
    private Toolbar toolbar;
    private TextView toolBarTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        toolBarTitle = findViewById(R.id.toolbar_title);
        viewPager = (NonSwipeViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

       // setSupportActionBar(toolbar);

        tabData = new ArrayList<>();
        tabData.add(new TabLayoutData(0, "Home", R.drawable.home, new HomeFragment()));
        tabData.add(new TabLayoutData(0, "Category", R.drawable.category, new CategoryFragment()));
        tabData.add(new TabLayoutData(0, "Cart", R.drawable.cart, new CartFragment()));
        tabData.add(new TabLayoutData(0, "Account", R.drawable.account, new AccountFragment()));

        pagerAdater = new ViewPagerAdapter(getSupportFragmentManager(), this, tabData);
        viewPager.setAdapter(pagerAdater);
        viewPager.setOffscreenPageLimit(5);

        tabLayout.setupWithViewPager(viewPager);
        pagerAdater.changeCustomeTab(tabLayout);
        pagerAdater.setTabIcon(0, R.drawable.home, Color.parseColor("#4dbff4"), 0);

        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        pagerAdater.setTabIcon(tab.getPosition(), R.drawable.home, Color.parseColor("#4dbff4"), 0);
                        toolBarTitle.setText("Sky Online Shop");
                        break;
                    case 1:
                        pagerAdater.setTabIcon(tab.getPosition(), R.drawable.category, Color.parseColor("#4dbff4"), 0);
                        toolBarTitle.setText("Category");
                        break;
                    case 2:
                        pagerAdater.setTabIcon(tab.getPosition(), R.drawable.cart, Color.parseColor("#4dbff4"), 0);
                        toolBarTitle.setText("Cart");
                        break;
                    case 3:
                        pagerAdater.setTabIcon(tab.getPosition(), R.drawable.account, Color.parseColor("#4dbff4"), 0);
                        toolBarTitle.setText("Account");
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        pagerAdater.setTabIcon(tab.getPosition(), R.drawable.home, Color.parseColor("#0d0c0d"), 0);
                        break;
                    case 1:
                        pagerAdater.setTabIcon(tab.getPosition(), R.drawable.category, Color.parseColor("#0d0c0d"), 0);
                        break;
                    case 2:
                        pagerAdater.setTabIcon(tab.getPosition(), R.drawable.cart, Color.parseColor("#0d0c0d"), 0);
                        break;
                    case 3:
                        pagerAdater.setTabIcon(tab.getPosition(), R.drawable.account, Color.parseColor("#0d0c0d"), 0);
                        break;
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

}
