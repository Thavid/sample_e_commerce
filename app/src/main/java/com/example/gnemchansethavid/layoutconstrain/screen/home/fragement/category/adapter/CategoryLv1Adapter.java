package com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.category.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gnemchansethavid.layoutconstrain.R;
import com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.category.CategoryFragment;
import com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.category.entity.Category;

import java.util.List;

public class CategoryLv1Adapter extends RecyclerView.Adapter<CategoryLv1Adapter.MyViewHolder> {
    private List<Category> listData;
    private Context context;
    private int position;
    private CategoryFragment categoryFragment = new CategoryFragment();
    public CategoryLv1Adapter(List<Category> listData, Context context) {
        this.listData = listData;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.category1_layout,viewGroup,false);
        return new MyViewHolder(view,context);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, final int i) {
        final Category category = listData.get(i);
        myViewHolder.cateLogo.setImageResource(category.getImage());
        myViewHolder.txtName.setText(category.getCateName());

        myViewHolder.txtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                for(int i=0;i<myViewHolder.txtName.getText().length();i++){
                    if(Character.toString(myViewHolder.txtName.getText().charAt(i)).equals("")){
                        myViewHolder.txtName.append("\n");
                    }
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

        myViewHolder.cate1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //listData.get(i).setCheck(true);
                position = i;
                notifyDataSetChanged();
                categoryFragment.showToast(context,i);
            }
        });

        if(position == i){
            myViewHolder.cate1.setBackgroundColor(Color.parseColor("#ffffff"));
            myViewHolder.txtName.setTextColor(Color.parseColor("#4dbff4"));
        }else{
            myViewHolder.cate1.setBackgroundColor(Color.parseColor("#e2e1e1"));
            myViewHolder.txtName.setTextColor(Color.parseColor("#0d0c0d"));
        }
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        LinearLayout cate1;
        ImageView cateLogo;
        TextView txtName;
        public MyViewHolder(@NonNull View itemView, final Context context) {
            super(itemView);
            cateLogo = itemView.findViewById(R.id.cateLv1Logo);
            txtName = itemView.findViewById(R.id.cateLv1Name);
            cate1 = itemView.findViewById(R.id.cate1);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(), getAdapterPosition(), Toast.LENGTH_SHORT).show();
                    Log.e("ooooo",getAdapterPosition()+"");
                }
            });
        }
    }
}
