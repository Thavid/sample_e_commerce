package com.example.gnemchansethavid.layoutconstrain.screen.home.adapter;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gnemchansethavid.layoutconstrain.R;
import com.example.gnemchansethavid.layoutconstrain.screen.home.entity.TabLayoutData;

import java.util.ArrayList;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private Context context;
    private ArrayList<TabLayoutData> listData;
    private TabLayout tabLayout;
    private ArrayList<View> views;

    public ViewPagerAdapter(FragmentManager fm, Context context, ArrayList<TabLayoutData> listData) {
        super(fm);
        this.context = context;
        this.listData = listData;
        views = new  ArrayList<>();
    }

    @Override
    public Fragment getItem(int i) {
        return listData.get(i).getFragment();
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;

    }
    public void changeCustomeTab(TabLayout tabLayout) {
        this.tabLayout = tabLayout;
        for (int i = 0; i < listData.size(); i++)
            tabLayout.getTabAt(i).setCustomView(getContentView(i));
    }

    public void setTabIcon(int position, int icon, int color, int num){
        tabLayout.getTabAt(position).setCustomView(setContentView(position, icon, color, num));
    }

    public View setContentView(int position, int icon, int color, int num){
        ImageView imageView = views.get(position).findViewById(R.id.tab_icon);
        imageView.setImageResource(icon);
        TextView txttitle = views.get(position).findViewById(R.id.txtTitle);
        txttitle.setTextColor(color);
        return views.get(position);
    }

    public View getContentView(int position){
        View view = LayoutInflater.from(context).inflate(R.layout.customtab_layout, null);
        TextView txtTitle = view.findViewById(R.id.txtTitle);
        ImageView imageView = view.findViewById(R.id.tab_icon);
        txtTitle.setText(listData.get(position).getTitle());
        imageView.setImageResource(listData.get(position).getIcon());
        views.add(view);
        return view;
    }
}
