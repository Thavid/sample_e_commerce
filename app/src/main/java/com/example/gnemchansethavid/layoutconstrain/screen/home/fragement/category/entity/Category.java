package com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.category.entity;

public class Category {
    private int image;
    private String cateName;
    private boolean check;

    public Category(int image, String cateName) {
        this.image = image;
        this.cateName = cateName;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getCateName() {
        return cateName;
    }

    public void setCateName(String cateName) {
        this.cateName = cateName;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    @Override
    public String toString() {
        return "Category{" +
                "image=" + image +
                ", cateName='" + cateName + '\'' +
                '}';
    }
}
