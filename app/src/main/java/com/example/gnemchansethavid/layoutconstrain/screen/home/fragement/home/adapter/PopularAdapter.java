package com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.home.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gnemchansethavid.layoutconstrain.R;
import com.example.gnemchansethavid.layoutconstrain.screen.detail.ProductDetail;
import com.example.gnemchansethavid.layoutconstrain.screen.home.fragement.home.entity.Product;

import java.util.List;

public class PopularAdapter extends RecyclerView.Adapter<PopularAdapter.MyViewHolder> {
    private Context context;
    private List<Product> listData;

    public PopularAdapter(Context context, List<Product> listData) {
        this.context = context;
        this.listData = listData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_layout,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Product product = listData.get(i);
        myViewHolder.txtName.setText(product.getName());
        myViewHolder.txtCode.setText(product.getCode());
        myViewHolder.txtPrice.setText(product.getPrice());
        myViewHolder.imageLogo.setImageResource(product.getImage());

        myViewHolder.cartView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,ProductDetail.class);
                context.startActivity(intent);
            }

        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView txtName,txtPrice,txtCode;
        private ImageView imageLogo;
        private CardView cartView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtName);
            txtCode = itemView.findViewById(R.id.txtCode);
            txtPrice = itemView.findViewById(R.id.txtPrice);
            imageLogo = itemView.findViewById(R.id.productLogo);
            cartView = itemView.findViewById(R.id.productCard);
        }
    }
}
